﻿using AutoMapper;
using dotnet_api_demo.Data;
using dotnet_api_demo.Data.Entities;
using dotnet_api_demo.Model;
using dotnet_api_demo.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_api_demo.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IUserManagerRepository _repository;
        private readonly ILogger<UsersController> _logger;
        private readonly IMapper _mapper;

        public UsersController(IUserManagerRepository repository, ILogger<UsersController> logger, IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Return all users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<IEnumerable<User>> GetUsers()
        {
            try
            {
                var users = _repository.GetAllUsers();
                if (users != null)
                    return Ok(_mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>(users));
                else
                {
                    _logger.LogError($"Not Found");
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get users: {ex}");
                return BadRequest("Failed to get users");
            }
        }

        /// <summary>
        /// Return user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<UserViewModel> GetUserById(int id)
        {
            try
            {
                var user = _repository.GetUserById(id);
                if (user != null)
                    return Ok(_mapper.Map<User, UserViewModel>(user));
                else
                {
                    _logger.LogError($"User specified by id = {id} not found");
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get user by id: {ex}");
                return BadRequest("Failed to get user by id");
            }
        }

        /// <summary>
        /// Return user by username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpGet("{username}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<User> GetUserByUsername(string username)
        {
            try
            {
                var user = _repository.GetUserByUsername(username);
                if (user != null)
                    return Ok(_mapper.Map<User, UserViewModel>(user));
                else
                {
                    _logger.LogError($"User specified by username = {username} not found");
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get user by username: {ex}");
                return BadRequest("Failed to get user by username");
            }
        }

        /// <summary>
        /// Post new user
        /// </summary>
        /// <param name="userViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult PostUser([FromBody] UserViewModel userViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _mapper.Map<UserViewModel, User>(userViewModel);
                    if (user.Id != 0)
                        return BadRequest("Please do not set the value of id.");
                    if (_repository.IsExist(user.Username))
                        return StatusCode(409, $"User '{user.Username}' already exists.");

                    user.Password = PasswordEncrypter.HashPassword(user.Password);
                    _repository.AddUser(user);
                    _logger.LogInformation("User was successfully saved");

                    var savedUserViewModel = _mapper.Map<User, UserViewModel>(user);
                    return Created($"/api/users/{savedUserViewModel.Id}", savedUserViewModel);
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to post user: {ex}");
                return StatusCode(StatusCodes.Status500InternalServerError, "Error inserting data");
            }

        }

        /// <summary>
        /// Update existing user
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userViewModel"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult UpdateUser(int id, [FromBody] UserViewModel userViewModel)
        {
            try
            {
                var user = _mapper.Map<UserViewModel, User>(userViewModel);
                if (id != user.Id)
                {
                    _logger.LogError($"Given id and given user's is does not match");
                    return StatusCode(409, $"User id mismatch.");
                }

                var existingUser = _repository.GetUserById(id);
                if (existingUser == null)
                {
                    _logger.LogError($"User specified by id = {id} not found");
                    return NotFound();
                }
                if (!existingUser.Username.Equals(user.Username))
                {
                    _logger.LogError($"It is not allowed to change username");
                    return StatusCode(409, $"It is not allowed to change username");
                }

                user.Password = PasswordEncrypter.HashPassword(user.Password);

                _repository.UpdateUser(existingUser, user);

                _logger.LogInformation("User was successfully updated");
                return Ok(_mapper.Map<User, UserViewModel>(user));
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }
        }

        /// <summary>
        /// Delete existing user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult DeleteUser(int id)
        {
            try
            {
                var user = _repository.GetUserById(id);
                if (user == null)
                {
                    _logger.LogError($"User specified by id = {id} not found");
                    return NotFound();
                }
                _repository.DeleteUser(user);
                _logger.LogInformation("User was successfully deleted");
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleting data");
            }
        }

        [HttpGet("is/{username}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult IsGivenUsernameTaken(string username)
        {
            try
            {
                var user = _repository.GetUserByUsername(username);
                if (user != null)
                    return Ok(true);
                else
                {
                    return Ok(false);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get user by username: {ex}");
                return BadRequest("Failed to get user by username");
            }
        }
    }
}
