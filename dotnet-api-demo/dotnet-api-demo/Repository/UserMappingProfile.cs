﻿using AutoMapper;
using dotnet_api_demo.Data.Entities;
using dotnet_api_demo.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_api_demo.Model
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            // UserViewModel is the data transfer object and User is the Model
            // Convert DTO to Model and vice versa
            CreateMap<User, UserViewModel>().ReverseMap();
        }
    }
}
