﻿using dotnet_api_demo.Data.Entities;
using dotnet_api_demo.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace dotnet_api_demo.Data
{
    public interface IUserManagerRepository
    {
        IEnumerable<User> GetAllUsers();

        User GetUserByUsername(string username);

        User GetUserById(int id);

        void AddUser(User user);
        
        bool SaveAll();

        void UpdateUser(User userToUpdate, User user);

        void DeleteUser(User userToDelete);

        bool IsExist(string username);

        bool IsExist(int id);
    }
}