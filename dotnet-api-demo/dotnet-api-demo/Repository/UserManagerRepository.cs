﻿using dotnet_api_demo.Data.Entities;
using dotnet_api_demo.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_api_demo.Data
{
    public class UserManagerRepository : IUserManagerRepository
    {
        private readonly UserManagerContext _ctx;
        private readonly ILogger<UserManagerRepository> _logger;

        public UserManagerRepository(UserManagerContext ctx, ILogger<UserManagerRepository> logger)
        {
            _ctx = ctx;
            _logger = logger;
        }

        /// <summary>
        /// Return list of all users
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> GetAllUsers()
        {
            try
            {
                _logger.LogInformation("GetAllUsers method called");
                return _ctx.Users.ToList();
            }
            catch(Exception ex)
            {
                _logger.LogError($"Failed to get all useres: {ex}");
                return null;
            }

        }

        /// <summary>
        /// Return user by username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public User GetUserByUsername(string username)
        {
            try
            {
                _logger.LogInformation("GetUserByUsername method called");
                return _ctx.Users.FirstOrDefault(user => user.Username.Equals(username));
            }
            catch (Exception)
            {
                _logger.LogError($"Failed to get user by username: {0}");
                return null;
            }
        }

        /// <summary>
        /// Return user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetUserById(int id)
        {
            try
            {
                _logger.LogInformation("GetUserById method called");
                return _ctx.Users.FirstOrDefault(user => user.Id == id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get user by id: {ex}");
                return null;
            }
        }

        /// <summary>
        /// Add new user
        /// </summary>
        /// <param name="user"></param>
        public void AddUser(User user)
        {
            try
            {
                _logger.LogInformation("AddUser method called");
                _ctx.Add(user);
                SaveAll();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to add user: {ex}");
                throw ex;
            }
        }

        /// <summary>
        /// Update given user
        /// </summary>
        /// <param name="userToUpdate">old user</param>
        /// <param name="user">new user</param>
        public void UpdateUser(User userToUpdate, User user)
        {
            try
            {
                _logger.LogInformation("UpdateUser method called");
                _ctx.Entry(userToUpdate).CurrentValues.SetValues(user);
                SaveAll();
            }            
            catch (Exception ex)
            {
                _logger.LogError($"Failed to update user: {ex}");
                throw ex;
            }
        }

        /// <summary>
        /// Delete specified user
        /// </summary>
        /// <param name="User"></param>
        public void DeleteUser(User user)
        {
            try
            {
                _logger.LogInformation("DeleteUser method called");
                _ctx.Remove(user);
                SaveAll();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to delete user: {ex}");
                throw ex;
            }
        }

        /// <summary>
        /// Save all changes and return true if there was any changes
        /// </summary>
        /// <returns></returns>
        public bool SaveAll()
        {
            _logger.LogInformation("Save changes...");
            int numberOfChanges = _ctx.SaveChanges();
            bool isChange = numberOfChanges > 0;
            if (isChange)
                _logger.LogInformation($"There was/were {numberOfChanges} change(s).");
            else
                _logger.LogInformation("There were no changes.");
            return isChange;
        }

        public bool IsExist(string username)
        {
            return GetUserByUsername(username) != null;
        }

        public bool IsExist(int id)
        {
            return GetUserById(id) != null;
        }
    }
}
