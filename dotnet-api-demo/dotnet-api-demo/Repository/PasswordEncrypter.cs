﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnet_api_demo.Model
{
    public static class PasswordEncrypter
    {
        /// <summary>
        /// Return hashed string
        /// </summary>
        /// <param name="password">string to hash</param>
        /// <returns></returns>
        public static string HashPassword(string password)
        {
            // Salt
            string saltString = "Ring";
            byte[] salt = Encoding.ASCII.GetBytes(saltString);
            
            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return hashed;
        }
    }
}
