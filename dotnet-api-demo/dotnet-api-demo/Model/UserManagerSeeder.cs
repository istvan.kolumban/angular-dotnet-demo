﻿using dotnet_api_demo.Data.Entities;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace dotnet_api_demo.Data
{
    public class UserManagerSeeder
    {
        private readonly UserManagerContext _ctx;
        private readonly IWebHostEnvironment _env;

        public UserManagerSeeder(UserManagerContext ctx, IWebHostEnvironment env)
        {
            _ctx = ctx;
            _env = env;
        }

        public void Seed()
        {
            _ctx.Database.EnsureCreated();

            if (!_ctx.Users.Any())
            {
                // Need to create sample data
                var filePath = Path.Combine(_env.ContentRootPath, "Model", "Data", "users.json");
                var json = File.ReadAllText(filePath);
                var users = JsonSerializer.Deserialize<IEnumerable<User>>(json);

                _ctx.Users.AddRange(users);

                _ctx.SaveChanges();
            }
        }
    }
}
