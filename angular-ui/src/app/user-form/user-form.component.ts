import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User, UserService } from 'src/app/services/user.service';
import { AsyncService } from '../validators/user-validator';

export const passwordsValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const password = control.get('password');
  const cpassword = control.get('cpassword');
  return password && cpassword && password.value === cpassword.value ? null : { areEqual: true };
};

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  submitButtonText = '';
  userToCreateOrEdit!: User;
  isOpendForNewUser!: boolean;
  shouldClearPasswordInputs!: boolean;
  confirmPassword = '';

  userForm!: FormGroup;
  get username(): AbstractControl {
    return this.userForm.get('username')!;
  }

  get password(): AbstractControl {
    return this.userForm.get('password')!;
  }

  get cpassword(): AbstractControl {
    return this.userForm.get('cpassword')!;
  }

  get fname(): AbstractControl {
    return this.userForm.get('fname')!;
  }

  get lname(): AbstractControl {
    return this.userForm.get('lname')!;
  }

  get email(): AbstractControl {
    return this.userForm.get('email')!;
  }

  get phone(): AbstractControl {
    return this.userForm.get('phone')!;
  }

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private asyncService: AsyncService) {

  }

  ngOnInit(): void {
    const username = String(this.route.snapshot.paramMap.get('username'));
    this.userToCreateOrEdit = { id: 0, username: '', password: '', firstName: '', lastName: '', email: '', phone: '', };

    if (username !== 'null') {
      this.isOpendForNewUser = false;
      this.shouldClearPasswordInputs = true;
      this.userService.getUserByUsername(username).subscribe((user: any) => {
        console.log(JSON.stringify(user));
        this.userToCreateOrEdit = {
          id: user.id,
          username: user.username,
          password: user.password,
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          phone: user.phone,
        };
        this.confirmPassword = user.Password;
        this.updateValues(this.userToCreateOrEdit);
      });
      this.submitButtonText = 'Save';
    } else {
      this.isOpendForNewUser = true;
      this.shouldClearPasswordInputs = false;
      this.submitButtonText = 'Create';
    }    
    this.setUserForm();
  }

  // navigate back without save
  onBack(): void {
    this.router.navigate(['/users']);
  }

  // listener for fomr submit
  onSubmit(): void {
    this.userToCreateOrEdit.username = this.username.value;
    this.userToCreateOrEdit.password = this.password.value;
    this.userToCreateOrEdit.firstName = this.fname.value;
    this.userToCreateOrEdit.lastName = this.lname.value;
    this.userToCreateOrEdit.email = this.email.value;
    this.userToCreateOrEdit.phone = this.phone.value;

    if (this.isOpendForNewUser) {
      this.userService.createUser(this.userToCreateOrEdit).subscribe(user => {this.router.navigate(['/users'])});
    }
    else {
      this.userService.updateUser(this.userToCreateOrEdit).subscribe(user => {this.router.navigate(['/users'])});;
    }
  }

  setUserForm(): void {
    this.userForm = this.fb.group({
      username: [{ value: this.userToCreateOrEdit.username, disabled: !this.isOpendForNewUser }, [Validators.required, Validators.minLength(4)], [this.asyncService.usernameValidator()]],
      password: [this.userToCreateOrEdit.password, [Validators.required, Validators.minLength(5)]],
      cpassword: [this.confirmPassword, [Validators.required]],
      fname: [this.userToCreateOrEdit.firstName, Validators.required],
      lname: [this.userToCreateOrEdit.lastName, Validators.required],
      email: [this.userToCreateOrEdit.email, [Validators.required, Validators.email]],
      phone: [this.userToCreateOrEdit.phone, [Validators.required, Validators.pattern("[0-9]{10}")]],
    },
    { validators: passwordsValidator });
  }

  updateValues(user: User) {
    this.userForm.setValue({
      username: user.username,
      password: user.password,
      cpassword: user.password,
      fname: user.firstName,
      lname: user.lastName, 
      email: user.email,
      phone: user.phone
    });
  }

  onPasswordInputClicked(){
    if (!this.isOpendForNewUser && this.shouldClearPasswordInputs){
      this.shouldClearPasswordInputs = false;
      this.userForm.patchValue({
        password: '',
        cpassword: ''
      });
    }
  }

  get diagnostic() { return JSON.stringify(this.userToCreateOrEdit); }
}
