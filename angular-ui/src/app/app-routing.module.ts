import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserFormComponent } from './user-form/user-form.component';
import { UserManagerComponent } from './user-manager/user-manager.component';

const routes: Routes = [
  {path: 'users', component: UserManagerComponent},
  {path: 'users/new', component: UserFormComponent},
  {path: 'users/:username', component: UserFormComponent},
  {path: '', redirectTo: 'users', pathMatch: 'full'},
  {path: '**', redirectTo: 'users', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
