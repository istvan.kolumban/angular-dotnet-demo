import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { User, UserService } from '../services/user.service';

@Component({
  templateUrl: './user-manager.component.html',
  styleUrls: ['./user-manager.component.css']
})
export class UserManagerComponent implements OnInit {

  // searched item
  searchedText;

  // reference to the table
  @ViewChild(MatTable)
  table!: MatTable<any>;

  // displayed column names
  displayedColumns: string[] = ['username', 'password', 'firstName', 'lastName', 'email', 'phone', 'actions'];

  // data contains all showed markers in table
  data!: User[];

  constructor(private userService: UserService, private router: Router) {
    this.searchedText ='';
    this.userService.getUsers().subscribe((users: any) => {
      this.data = users;
    });
  }

  ngOnInit(): void {
  }

  onEditClick(user: User): void {
    this.router.navigate(['/users', user.username]);
  }

  onCreateClick(): void {
    this.router.navigate(['/users/new']);
  }

  onDeleteClick(user: User): void {
    this.userService.deleteUser(user).subscribe(() => this.updateTable(this.searchedText));
  }

  onChangeSearch(): void {
    this.updateTable(this.searchedText);
  }

  // Update the table after data might changed
  updateTable(searchedText: string): void {
    if (searchedText.length > 0){
      this.userService.getUsers().subscribe((users: any) => {
        this.data = users.filter(
          (x: User) =>
            x.username.includes(this.searchedText) ||
            x.firstName.toString().includes(this.searchedText) ||
            x.lastName.toString().includes(this.searchedText) ||
            x.email.toString().includes(this.searchedText) ||
            x.phone.toString().includes(this.searchedText)
        );
      });
    } else {
      this.userService.getUsers().subscribe((users: any) => {
        console.log(JSON.stringify(users));
        this.data = users;
      });
    }
    this.table.renderRows();
  }
}
