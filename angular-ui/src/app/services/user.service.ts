import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject, from, Observable, of, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

export interface User {
  id: number; // don't set or change this, the server will do it.
  username: string; // this has to be uniq
  password: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = "https://localhost:5001/api/users";

  constructor(private http: HttpClient, private router: Router) {}

  // all users
  getUsers():  Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl).pipe(
      catchError((error) => this.handleError(error,this.router))
    );
  }

  // get user by username
  getUserByUsername(username: string): Observable<User> {
    const url = this.baseUrl + "/" + username;
    return this.http.get<User>(url).pipe(
      catchError((error) => this.handleError(error,this.router))
    );
  }

  // delete given user
  deleteUser(user: User): Observable<{}> {
    const url = `${this.baseUrl}/${user.id}`
    return this.http.delete(url).pipe(
      catchError((error) => this.handleError(error,this.router))
    );
  }

  // create new user
  createUser(user: User): Observable<User>  {
    return this.http.post<User>(this.baseUrl, user).pipe(
      catchError((error) => this.handleError(error,this.router))
    );
  }

  // updated given user
  updateUser(user: User): Observable<User> {
    const url = `${this.baseUrl}/${user.id}`
    return this.http.put<User>(url, user).pipe(
      catchError((error) => this.handleError(error,this.router))
    );
  }

  // check if given ursername is already taken
  isUsernameTaken(username: string): Observable<Object> {
    const url = `${this.baseUrl}/is/${username}`
    return this.http.get(url).pipe(
      catchError((error) => this.handleError(error,this.router))
    );
  }

  handleError(error: HttpErrorResponse, router: Router) {
  if (error.status === 0) {
    console.error('An error occurred:', error.error);
    alert(`An error occurred: Probably the server is not started yet.`);
  } else {
    console.error(
      `Backend returned code ${error.status}, ` +
      `body was: ${error.error}`);
    if (error.status == 404){
      alert("User not found!");
      router.navigate(['/users']);
    }
  }
  // Return an observable with a user-facing error message.
  return throwError(
    'Something bad happened; please try again later.');
  }

}
