import { Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';
import { UserService } from '../services/user.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AsyncService {
  constructor(private userService: UserService) {}

  usernameValidator(): ValidatorFn {
    return (
      control: AbstractControl
    ): Observable<{ [key: string]: any } | null> => {
      return this.userService.isUsernameTaken(control.value).pipe(
        map((isUnique) => (isUnique ? { unique: true } : null))
      );
    };
  }
}
